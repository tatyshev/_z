//# ============================================================
//#  Declatations
//# ============================================================
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    include = require('gulp-include'),
    stylus = require('gulp-stylus'),
    minifycss = require('gulp-minify-css'),
    prefix = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    lr = require('gulp-livereload');

var LISTENER = false;
var logerr = function(error){ console.log(error); };
var reload = function(){ if(LISTENER) lr.changed(); };

//# ============================================================
//#  Task - Default
//# ============================================================
gulp.task('default', function() {
    gulp.start('styles');
    gulp.start('scripts');
});


//# ============================================================
//#  Task - Styles
//# ============================================================
gulp.task('styles', function() {
    gulp.src('assets/styles/index.styl')
        .pipe(stylus().on('error', logerr))
        .pipe(concat('styles.css'))
        .pipe(prefix("last 4 version", "ie >= 9"))
        .pipe(minifycss({
            keepSpecialComments: 0,
            root: 'assets'
        }))
        .pipe(gulp.dest('assets'))
        .on('end', reload);
});


//# ============================================================
//#  Task - Scripts
//# ============================================================
gulp.task('scripts', function(){
    gulp.src('assets/scripts/index.js')
        .pipe(concat('scripts.js'))
        .pipe(include().on('error', logerr))
        .pipe(uglify().on('error', logerr))
        .pipe(gulp.dest('assets'))
        .on('end', reload);
});


//# ============================================================
//#  Task - Live
//# ============================================================
gulp.task('live', function(){
    // Once compile
    gulp.start('default');

    // Basic watchers
    gulp.watch(['assets/styles/**/*.styl'], ['styles']);
    gulp.watch(['assets/scripts/**/*.js'], ['scripts']);

    // Php template change detect
    gulp.watch(['*.php', 'templates/*.php', 'includes/*.php'], [reload]);

    lr.listen();
    LISTENER = true;
});
