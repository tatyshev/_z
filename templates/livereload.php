<?php if(current_user_can('manage_options')): ?>

    <script type="text/html" id="livereload_template">
        <li>
            <a href='#' class='ab-item' id="livereload_switcher">
                <label><input type="checkbox" style="position:relative; top:2px;"/> LiveReload</label>
            </a>
        </li>
    </script>

    <script>
        jQuery(function($){
            var ls = localStorage,
                lr = '_z_livereload',
                tpl = $('#livereload_template').text();

            $('#wp-admin-bar-top-secondary').append(tpl);
            var check = $('#livereload_switcher input');

            if(ls[lr] == 'true'){
                check.prop('checked', true);
                var source = "<?php echo '//'.gethostname().':35729/livereload.js'?>"
                $('head').append( $('<script/>', { src: source, async: true }) );
            } else {
                check.prop('checked', false);
            }

            check.change(function(){
                ls[lr] = this.checked;
                location.reload();
            });
        });
    </script>
    
<?php endif; ?>