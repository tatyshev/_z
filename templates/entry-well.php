<?php if ( 'post' == get_post_type() ) :?>
    <span class="cat-links">
        <?php printf(__('Posted on %s', '_z'), get_the_category_list(', ')); ?>
    </span>
<?php endif;?>

<?php if(get_the_tag_list()): ?>
    <span class="tags-links">
        <?php printf('Tagged %s', get_the_tag_list('', ', ')); ?>
    </span>
<?php endif;?>

<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
    <span class="comments-link">
        <?php
            if(!is_singular() && !is_page()){
                comments_popup_link(
                    __( 'Leave a comment', '_z' ),
                    __( '1 Comment', '_z' ),
                    __( '% Comments', '_z' )
                );
            }
        ?>
    </span>
<?php endif; ?>
