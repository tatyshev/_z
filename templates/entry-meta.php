<time class="published" datetime="<?php echo get_the_time('c'); ?>">
    <?php _e('Published on ', '_z')?> <?php echo get_the_date(); ?>
</time>
<p class="byline author">
    <?php echo __('By', 'roots'); ?>
    <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author">
        <?php echo get_the_author(); ?>
    </a>
</p>