<?php
    if(post_password_required()) return;

    if (is_singular() and comments_open()) {
        wp_enqueue_script('comment-reply');
    }
?>

<div id="comments" class="comments">
    <?php if (have_comments()): ?>

        <h3 class="comments__header">
            <?php
                $_z_cm = get_comments_number();
                if($_z_cm == 1) {
                    printf(__('One Response to %s', '_z'), '&laquo;'.get_the_title().'&raquo;');
                } else {
                    printf(__('%1$s Responses to %2$s', '_z'), '&laquo;'.get_the_title().'&raquo;', $_z_cm);
                }
            ?>
        </h3>

        <?php if (get_comment_pages_count() > 1 && get_option( 'page_comments' )): ?>
            <nav id="comments__nav" class="comments__nav" role="navigation">
                <ul>
                    <li class="comments__nav__prev"><?php previous_comments_link( __( '&larr; Older Comments', '_z' ) ); ?></li>
                    <li class="comments__nav__next"><?php next_comments_link( __( 'Newer Comments &rarr;', '_z' ) ); ?></li>
                </ul>
            </nav>
        <?php endif; ?>

        <ol>
            <?php
                wp_list_comments(array(
                    'style'      => 'ol',
                    'short_ping' => true,
                ));
            ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option( 'page_comments' )): ?>
            <nav id="comments__nav" class="comments__nav" role="navigation">
                <ul>
                    <li class="comments__nav__prev"><?php previous_comments_link( __( '&larr; Older Comments', '_z' ) ); ?></li>
                    <li class="comments__nav__next"><?php next_comments_link( __( 'Newer Comments &rarr;', '_z' ) ); ?></li>
                </ul>
            </nav>
        <?php endif; ?>

    <?php endif; ?>


    <?php if (!comments_open()): ?>
        <p class="comments__none">
            <?php _e('Comments are closed.', '_z'); ?>
        </p>
    <?php else: ?>
        <?php comment_form() ?>
    <?php endif; ?>

</div>