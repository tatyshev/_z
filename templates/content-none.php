<section class="content__none">
    <p><?php _e('Sorry, no results were found.', '_z'); ?></p>
    <?php get_search_form(); ?>
</section>