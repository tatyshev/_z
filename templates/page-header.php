<header id="page__header" class="page__header" role="banner">

    <div class="branding">
        <h1 class="branding__title">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <?php bloginfo( 'name' ); ?>
            </a>
        </h1>

        <div class="branding__description">
            <?php bloginfo( 'description' ); ?>
        </div>
    </div>

    <nav id="nav-main" class="nav-main" role="navigation">
        <?php
            wp_nav_menu(array(
                'theme_location' => 'primary',
                'container' => false
            ));
        ?>
    </nav>

</header>