<?php
    if ($GLOBALS['wp_query']->max_num_pages < 2) {
        return;
    }
?>

<nav class="pagination">
    <ul class="pagination__links" role="navigation">
        <?php if (get_next_posts_link()) : ?>
            <li class="pagination__prev">
                <?php next_posts_link(__( '<span class="meta-nav">&larr;</span> Older posts', '_z')); ?>
            </li>
        <?php endif; ?>

        <?php if (get_previous_posts_link()) : ?>
            <li class="pagination__next">
                <?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', '_z')); ?>
            </li>
        <?php endif; ?>
    </ul>
</nav>