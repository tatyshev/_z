<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">

    <input class="search-form__field"
           placeholder="<?php _e('What to look for?', '_z'); ?>"
           value="<?php echo get_search_query(); ?>"
           name="s"
           type="search">

    <input class="search-form__submit" value="<?php _e('Search','_z'); ?>" type="submit">

</form>