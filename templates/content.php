<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <header class="entry-header">
        <?php if(is_singular()): ?>
            <h1 class="entry-title"><?php the_title()?></h1>
        <?php else: ?>
            <h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title()?></a></h1>
        <?php endif;?>

        <div class="entry-meta">
            <?php get_template_part('/templates/entry', 'meta'); ?>
        </div>
    </header>

    <div class="entry-content">
        <?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', '_z' ) ); ?>
        <?php
            wp_link_pages(array(
                'before' => '<nav class="page-nav"><p>' . __('Pages:', '_z'),
                'after' => '</p></nav>'
            ));
        ?>
    </div>

    <footer class="entry-footer">
        <?php get_template_part('templates/entry', 'well')?>
    </footer>

    <?php if(is_singular()): ?>
        <?php comments_template(); ?>
    <?php endif; ?>

</article>