<footer id="page__footer" class="page__footer" role="contentinfo">
    <div class="page-footer__info">
        &copy; <?php echo date('Y') ?>
        <a href="<?php home_url()?>" rel="index"><?php bloginfo('name')?></a>
        <?php _e('All Rights Reserved', '_z')?>
    </div>
</footer>