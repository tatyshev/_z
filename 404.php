<?php get_header(); ?>

<main id="content" class="content" role="main">
    
    <section class="content__404">
        <header>
            <h1><?php _e( 'Oops! That page can&rsquo;t be found.', '_z' ); ?></h1>
        </header>

        <div>
            <p><?php _e( 'It looks like nothing was found at this location.', '_z' ); ?></p>
        </div>
    </section>

</main>

<?php get_footer(); ?>