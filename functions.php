<?php

require_once 'includes/utils.php';
require_once 'includes/setup.php';
require_once 'includes/filters.php';

require_once 'includes/scripts.php';
require_once 'includes/sidebars.php';
require_once 'includes/menus.php';