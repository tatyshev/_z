<?php

function _z_sidebars(){

    register_sidebar(array(
        'name'          => __( 'Sidebar Primary', '_z' ),
        'id'            => 'primary',
        'description'   => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ));

}

add_action('widgets_init', '_z_sidebars');
