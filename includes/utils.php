<?php

// Get url relative to theme folder
function _z_url($relative){
    $path = get_template_directory_uri();
    $relative = trim(trim($relative, '/'));
    return esc_url($path . '/' . $relative);
}

// Get url from assets folder
function _z_assets($url){
    $url = trim(trim($url, '/'));
    return _z_url('assets/' . $url);
}

// Print php objects to javascript console
function _z_log($obj){
    $trace = debug_backtrace();
    $trace = $trace[0];

    $group = $trace['file'].':'.$trace['line'];
    $group = str_replace('\\', '\\\\', $group);
    $id = uniqid();
    
    ob_start(); print_r($obj);
    $msg = ob_get_clean();

    echo '<script type="text/plain" id="'.$id.'">'.$msg.'</script>';
    echo '<script>';
    echo 'console.group("'.$group.'");';
    echo 'console.log(document.getElementById("'.$id.'").innerHTML);';
    echo 'console.groupEnd();';
    echo '</script>';
}