<?php
// -----------------------------------------------------------------------------
// Same markup for none attached menus
// -----------------------------------------------------------------------------
function _z_filter_page_menu($content) {
    $div_wrapper = '~^\<div class="menu">([\s\S]+?)\<\/div\>$~';
    $children_class = '~(\<ul.+?class=[\"\'].*?)children(.*?[\"\']>)~';
    $content = preg_replace($div_wrapper, '$1', $content);
    $content = preg_replace($children_class, '$1sub-menu$2', $content);
    return trim($content);
}
add_filter('wp_page_menu', '_z_filter_page_menu');


// -----------------------------------------------------------------------------
// Remove generator meta tag from header
// -----------------------------------------------------------------------------
function _z_remove_version() {
    return '';
}
add_filter('the_generator', '_z_remove_version');


// -----------------------------------------------------------------------------
// Remove 'text/css' from enqueued stylesheet
// -----------------------------------------------------------------------------
function _z_textcss_remove($tag) {
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}
add_filter('style_loader_tag', '_z_textcss_remove');


// -----------------------------------------------------------------------------
// Highlight Search Terms
// -----------------------------------------------------------------------------
function _z_search_highlight($content) {
    if(is_search()){
        $search_query = get_search_query();

        if( empty($search_query) ){
            return $content;
        }

        $keys = explode(' ', get_search_query());
        $keys = implode('|', array_filter(array_map('trim', $keys)) );
        $content = preg_replace('/('.$keys.')/iu', '<mark>\0</mark>', $content);
        return $content;
    }
    return $content;
}
add_filter('the_content', '_z_search_highlight');
add_filter('the_title', '_z_search_highlight');