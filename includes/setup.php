<?php

require_once 'utils.php';

function _z_setup(){
    load_theme_textdomain('_z', get_template_directory().'/languages');

    add_theme_support('automatic-feed-links' );

    add_theme_support('html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption'
    ));

    add_theme_support('post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link'
    ));

    add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', '_z_setup');

