<?php

function _z_menus(){

    register_nav_menus(array(
        'primary' => __( 'Primary Menu', '_z' ),
    ));

}

add_action('widgets_init', '_z_menus');