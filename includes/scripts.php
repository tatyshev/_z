<?php

require_once 'utils.php';

function _z_styles(){
    wp_enqueue_style('open-sans');
    wp_enqueue_style('_z_style', get_stylesheet_uri());
    wp_enqueue_style('_z_main_style', _z_assets('styles.css'));
}

function _z_scripts(){
    wp_enqueue_script('jquery');
    wp_enqueue_script('_z_main_script', _z_assets('scripts.js'));
}

add_action('wp_print_styles', '_z_styles');
add_action('wp_enqueue_scripts', '_z_scripts');
