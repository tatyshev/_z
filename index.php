<?php get_header(); ?>
<div id="page" class="page">

    <?php get_template_part('/templates/page', 'header'); ?>

    <div id="page-content" class="page-content">

        <main id="content" class="content" role="main">
        
            <?php $search_query = get_search_query(); ?>
            <?php if( is_search() && !empty($search_query) ): ?>
                <header class="search-header">
                    <h1>
                        <?php printf( __( 'Search Results for: %s', '_z' ), '<span>&laquo;' . get_search_query() . '&raquo;</span>' ); ?>
                    </h1>
                </header>
            <?php endif; ?>
        
            <?php
                if(have_posts()):
                    while(have_posts()): the_post();
                        get_template_part('/templates/content', get_post_format());
                    endwhile;
                    get_template_part('/templates/pagination');
                else:
                    get_template_part('/templates/content', 'none');
                endif;
            ?>
            
        </main>

        <?php get_template_part('templates/sidebar', 'primary'); ?>

    </div>

    <?php get_template_part('/templates/page', 'footer'); ?>

</div>
<?php get_footer(); ?>
